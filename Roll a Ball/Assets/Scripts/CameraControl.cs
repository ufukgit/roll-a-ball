using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour
{
    public GameObject ball;
    public Vector3 diffVector;
    // Start is called before the first frame update
    void Start()
    {
        diffVector = new Vector3(0f, 10f, -15f);
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = ball.transform.position + diffVector;
    }
}
