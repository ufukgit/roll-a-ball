using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class BallControl : MonoBehaviour
{
    // public veya private farki ne olacakti?
    // mono behaviour neden inherite edildi?
    // physic objesinin ne objesi oldugunu unityye belirttim
    // rigidbody componenti oldugu belirtildi 
    // get component methodu MonoBehaviour dan mi geldi?
    // instantiated ??
    // Update is called once per frame
    // fixed update ve update farki nedir?
    public int hiz;
    public int counter;
    public int objectNumber;
    public Vector3 physicVector;
    public Rigidbody physic;
    public Text pointText;
    public Text timeText;
    public Text gameOverText;
    public Text youWinText;
    public Button tryAgainButton;
    public Button startButton;
    private bool isGameStarted = false;
    private bool isGameOver = false;
    private bool isGameWon = false;
    public float currentTime = 30f;
    private float obstacleSpawnTimer = 5f; // Time between obstacle spawns
    private float currentObstacleSpawnTime = 0f;
    public GameObject obstaclePrefab; // Reference to the obstacle prefab
    void Start()
    {
        startButton.gameObject.SetActive(true);
        hiz = 5;
        objectNumber = 10;
        physic = GetComponent<Rigidbody>();
        startButton.onClick.AddListener(StartGame);
        tryAgainButton.onClick.AddListener(RestartGame);
        gameOverText.gameObject.SetActive(false);
        tryAgainButton.gameObject.SetActive(false);
    }
    void Update()
    {
        if (isGameStarted && !isGameWon)
        {
            currentTime -= Time.deltaTime;
            if (currentTime <= 0)
            {
                if (isGameOver || isGameWon)
                {
                    Time.timeScale = 0f;
                    tryAgainButton.gameObject.SetActive(true);
                }
            }
            else
            {
                // Update time text or perform other actions based on current time
                timeText.text = "Time: " + currentTime.ToString("F0");
            }
            currentObstacleSpawnTime += Time.deltaTime;
            if (currentObstacleSpawnTime >= obstacleSpawnTimer)
            {
                SpawnObstacle();
                currentObstacleSpawnTime = 0f;
            }
        }
        else if (isGameWon)
        {
            Time.timeScale = 0f;
            tryAgainButton.gameObject.SetActive(true);
        }
    }
    void SpawnObstacle()
    {
        float spawnX = Random.Range(-10f, 10f); // Adjust the range based on your stage size
        float spawnZ = Random.Range(-10f, 10f); // Adjust the range based on your stage size
        GameObject obstacle = Instantiate(obstaclePrefab, new Vector3(spawnX, 0.5f, spawnZ), Quaternion.identity);
        obstacle.AddComponent<BarrierControl>();
    }
    void RestartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        isGameStarted = true;
        currentTime = 33f;
        isGameOver = false;
        isGameWon = false;
        Time.timeScale = 1f;
        gameOverText.gameObject.SetActive(false);
        startButton.gameObject.SetActive(false);
        tryAgainButton.gameObject.SetActive(false);

    }
    void StartGame()
    {
        isGameStarted = true;
        currentTime = 33f;
        isGameOver = false;
        isGameWon = false;
        Time.timeScale = 1f;
        gameOverText.gameObject.SetActive(false);
        startButton.gameObject.SetActive(false);
        tryAgainButton.gameObject.SetActive(false);
    }
    void FixedUpdate()
    {
        if (isGameStarted)
        {
            float timeValue = (33 - Time.time);
            if (currentTime > 0)
            {
                timeText.text = "Time : " + currentTime;
            }
            else
            {
                isGameOver = true;
                gameOverText.gameObject.SetActive(true);
                timeText.text = "Time : 0";
            }
            if (counter == objectNumber)
            {
                isGameWon = true;
                youWinText.gameObject.SetActive(true);
            }
            if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W))
            {
                if (hiz < 15)
                    hiz++;
                else
                    hiz = 15;
            }
            // Decrease speed when the Down Arrow key is held down
            if (Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S))
            {
                if (hiz > -15)
                    hiz--;
                else
                    hiz = -15;
            }
            float horizontal = Input.GetAxisRaw("Horizontal");
            float vertical = Input.GetAxisRaw("Vertical");
            if (hiz >= 0)
                physicVector = new Vector3(horizontal, 0, vertical);
            else
                physicVector = new Vector3(horizontal * (-1), 0, vertical * (-1));

            physic.AddForce(physicVector * hiz);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        //Debug.Log("the Ball hit the Cube");
        //Destroy(other.gameObject);
        //Debug.Log("Point : " + counter);
        other.gameObject.SetActive(false);
        counter++;
        pointText.text = "Point : " + (counter * 5);
    }

    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.CompareTag("Plane"))
        {
            MoveBall();
        }
    }
    private void MoveBall()
    {
        float randomX = Random.Range(-8, 8);
        float randomZ = Random.Range(-8, 8);
        Vector3 newPosition = new Vector3(randomX, 0.2f, randomZ); // Move the ball 1 unit forward
        transform.position = newPosition;

    }
    /*Called when the ball and cube are out of contact
     private void OnTriggerExit(Collider other)
     {
         other.gameObject.SetActive(false);
     }

     Called whenever the ball and the cube are in contact
     private void OnTriggerStay(Collider other)
     {
         other.gameObject.SetActive(false);
     }*/
}
