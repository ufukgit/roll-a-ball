using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarrierControl : MonoBehaviour
{
    private float rotationSpeed = 30f;
    private float targetRotationAngle;

    void Start()
    {
        GenerateRandomRotation();
    }

    void Update()
    {
        RotateObject();
    }

    void RotateObject()
    {
        if (Mathf.Abs(transform.rotation.eulerAngles.y - targetRotationAngle) > 0.01f)
        {
            float step = rotationSpeed * Time.deltaTime;
            Quaternion targetRotation = Quaternion.Euler(0f, targetRotationAngle, 0f);
            transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, step);
        }
    }

    void GenerateRandomRotation()
    {
        targetRotationAngle = Random.Range(0f, 90f);
    }
}
