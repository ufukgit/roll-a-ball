using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeControl : MonoBehaviour
{
    public float placementInterval = 2.5f;
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("PlaceCubeRandomly", placementInterval, placementInterval);
    }
    void PlaceCubeRandomly()
    {
        float randomX = Random.Range(-9, 9);
        float randomY = Random.Range(0.7f, 1.0f);
        float randomZ = Random.Range(-9, 9);
        // Set the cube's position to the randomly generated values
        transform.position = new Vector3(randomX, randomY, randomZ);
    }
    // Update is called once per frame
    void Update()
    {
        Vector3 vec = new Vector3(15, 30, 45);
        transform.Rotate(vec * 0.02f);
        float time = Time.time;
    }
}
